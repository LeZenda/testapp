from django.db import models
from time import time
from django_fo import settings

def get_upload_file_name(instance, filename):
	return settings.UPLOAD_FILE_PATTERN % (str(time()).replace('.','_'),filename)

# Create your models here.
class Article(models.Model):
	title = models.CharField(max_length = 200)
	body = models.TextField()
	pub_date = models.DateTimeField('date published')
	likes = models.IntegerField(default = 0)
	thumbnail = models.FileField(upload_to = get_upload_file_name)

	def __unicode__(self):
		temp = str(self.id) + " | " + self.title
		return temp

	def get_thumbnail(self):
		temp = str(self.thumbnail)
		if not settings.DEBUG:
			temp = temp.replace('assets/', '')
		return temp

class Comment (models.Model):
	name = models.CharField(max_length = 200)
	body = models.TextField()
	pub_date = models.DateTimeField('date published')
	article = models.ForeignKey(Article)