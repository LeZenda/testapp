from django import forms
from models import Userprofile

class UserProfileForm(forms.ModelForm):

	class Meta:
		model = Userprofile
		fields = ('femail', 'favorite_name')