from django.db import models
from django.contrib.auth.models import User

class Userprofile(models.Model):
	user = models.OneToOneField(User)
	femail = models.BooleanField()
	favorite_name = models.CharField(max_length = 50)

# kinde like magic
User.profile = property(lambda u: Userprofile.objects.get_or_create(user = u)[0])
