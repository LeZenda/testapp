from django.conf.urls import patterns, include, url
from django_fo.forms import ContactForm1, ContactForm2, ContactForm3
from django_fo.views import ContactWizard
import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	# Examples:
	(r'^articles/', include('article.urls')),
	(r'^accounts/', include('userprofile.urls')),
	url(r'^accounts/login/$', 'django_fo.views.login'),
	url(r'^accounts/auth/$', 'django_fo.views.auth_view'),
	url(r'^accounts/logout/$', 'django_fo.views.logout'),
	url(r'^accounts/loggedin/$', 'django_fo.views.loggedin'),
	url(r'^accounts/invalid/$', 'django_fo.views.invalid'),
	url(r'^accounts/register/$', 'django_fo.views.regiser_user'),
	url(r'^accounts/register_success/$', 'django_fo.views.register_success'),
	url(r'^contacts/$', ContactWizard.as_view([ContactForm1, ContactForm2, ContactForm3])),


	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	url(r'^admin/', include(admin.site.urls)),
)

if not settings.DEBUG:
	from django.contrib.staticfiles.urls import staticfiles_urlpatterns

	urlpatterns += staticfiles_urlpatterns()