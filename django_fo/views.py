from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from forms import MyRegistrationForm
from django.contrib.formtools.wizard.views import SessionWizardView
from django.core.mail import send_mail


def login(request):
	c = {}
	c.update(csrf(request))
	return render_to_response('login.html', c)

def auth_view(request):
	username = request.POST.get('username','')
	password = request.POST.get('password','')
	user = auth.authenticate(username=username,password=password)

	if user:
		auth.login(request, user)
		return HttpResponseRedirect('/accounts/loggedin')
	
	return HttpResponseRedirect('/accounts/invalid')

def loggedin(request):
	return render_to_response('loggedin.html',{'name' : request.user.username})

def invalid(request):
	return render_to_response('invalid.html')

def logout(request):
	auth.logout(request)
	return render_to_response('logout.html')

def regiser_user(request):
	if request.method == 'POST':
		form = MyRegistrationForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/accounts/register_success')

	args = {}
	args.update(csrf(request))
	args['form'] = MyRegistrationForm()

	return render_to_response('register.html', args)

def register_success(request):
	return render_to_response('register_success.html')

class ContactWizard(SessionWizardView):
	template_name = 'contact_form.html'

	def done(self,form_list,**kwarg):
		form_data = process_form_data(form_list)

		return render_to_response('done.html', {'form_data': form_data})

def process_form_data(form_list):
	form_data = [form.cleaned_data for form in form_list]

	send_mail(form_data[0]['subject'],form_data[2]['message'],form_data[1]['sender'],['tarik02@ukr.net'], fail_silently = False)

	return form_data